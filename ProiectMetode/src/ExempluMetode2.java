public class ExempluMetode2 {
    public static void main(String[] args) {
        System.out.println("Intrare in program ...");
        Clasa1();
        Clasa2();
        Clasa3();
        Clasa4();
        System.out.println("Clasele primare sunt suplinite.");
        System.out.println("Iesire din program.");
    }

    public static void Clasa1() {
        System.out.println("A fost importata clasa 1");
        System.out.println("Litera A");
        System.out.println("Litera B");
        System.out.println("Litera C");
        System.out.println("Litera D");
    }

    public static void Clasa2() {
        System.out.println("A fost importata clasa 2");
        System.out.println("Litera A");
        System.out.println("Litera B");
        System.out.println("Litera C");
        System.out.println("Litera D");
    }

    public static void Clasa3() {
        System.out.println("A fost importata clasa 3");
        System.out.println("Litera A");
        System.out.println("Litera B");
        System.out.println("Litera C");
        System.out.println("Litera D");
    }

    public static void Clasa4() {
        System.out.println("A fost importata clasa 4");
        System.out.println("Litera A");
        System.out.println("Litera B");
        System.out.println("Litera C");
        System.out.println("Litera D");
    }

}
