public class Vehicle {
    int passengers, wheels, maxspeed, burnup;
    //declaram metaoda, care calculeaza distanta parcursa
    //metoda primeste parametrul i terval, care seteaza timpul
    //si nu returneaza nici o valoare (void)
    void distance (double interval){
        double value = maxspeed * interval;
        System.out.println("Va parcurge o distanta egala cu " + value + " km.");
        // distance (double interval)
        // vehicle class
    }
    public static void main (String[] args){
        Vehicle masina = new Vehicle();
        masina.passengers = 2;
        masina.wheels = 4;
        masina.maxspeed = 130;
        masina.burnup = 30;
        // alt exemplu al clasei vehicle
        Vehicle autobus = new Vehicle();
        autobus.passengers = 45;
        autobus.wheels = 4;
        autobus.maxspeed = 100;
        autobus.burnup = 25;

        //calcularea traseului parcurs in 0,5 ore
        double time = 0.5;

        System.out.print("Atomobilul cu " + masina.passengers + " pasageri ");
        masina.distance(time);
        System.out.print("Autobuzul cu " + autobus.passengers + " pasageri ");
        autobus.distance(time);
    }
}
